class Camera {

  PVector target; //where camera tries to move in world
  PVector shake_offset;
  PVector engine_offset = new PVector(0.15, 0); //part of shake offset determined by speed
  PVector world_position; //position of camera in world
  PVector position; //resulting position of camera with shake offset
  PVector slap_offset = new PVector(1, 0); //part of shake offset determined by explosions
  float slap_power = 0; //current amplitude of explosion induced shake
  float slap_lerp = 0.1; //speed at which explosion induced shake dies down
  float slap_strength = 20; //default power of explosion
  float lerp_speed = 0.1;//speed at which camera follows the target

  Camera() {
    world_position = new PVector(-200, -200).add(player.position);
    position = new PVector(0, -100).add(player.position);
    shake_offset = new PVector(0, 0);
  }

  void move() {

    //calculates where camera should go
    target  = new PVector(player.position.x, player.position.y).add(new PVector(player.speed.x*5, player.speed.y*5)).sub(new PVector(200, 200)).add(new PVector(player.direction.x*100, player.direction.y*100));

    //slowly moves the camera there
    world_position.x = lerp(world_position.x, target.x, lerp_speed);
    world_position.y = lerp(world_position.y, target.y, lerp_speed);

    //moves the shake offsets
    engine_offset.rotate(random(PI*0.8, PI*1.2));
    slap_offset.rotate(random(PI*0.8, PI*1.2));

    //lowers the amplitude
    slap_power = max(lerp(slap_power, 0, slap_lerp) - 0.1, 0);

    //calculates final position of camera
    shake_offset  = new PVector(engine_offset.x, engine_offset.y).mult(player.speed.mag()).add(new PVector(slap_offset.x, slap_offset.y).mult(slap_power));
    position = new PVector( world_position.x, world_position.y).add(shake_offset);
  }

  void slap() {

    //adds the default explosion power to current amplitude
    slap_power += slap_strength;
  }
}
