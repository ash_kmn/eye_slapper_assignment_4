//declares variables used by death screen
boolean death_screen = true; //should this screen be displayed
int dead_timer = 0; //how long has this screen been up

void draw_death_screen() {

  //counts how long this screen has been open
  dead_timer++;

  //draws the instruction text
  background(background);
  fill(text);
  noStroke();
  textSize(40);
  text("ram the enemies", 40, 40);
  text("dodge the bullets", 35, 90);
  text("mouse to aim", 60, 140);
  textSize(30);
  text("score: " + score, 15, 385);

  //draws player shape
  fill(player_body);
  beginShape();
  vertex(200, 260);
  vertex(230, 320);
  vertex(200, 310);
  vertex(170, 320);
  endShape(CLOSE);

  //draws arrow shapes
  stroke(player_body);
  strokeWeight(10);
  line(200, 220, 225, 280);
  line(200, 220, 175, 280);
  line(235, 300, 245, 320);
  line(245, 320, 235, 340);
  line(165, 300, 155, 320);
  line(155, 320, 165, 340);

  //draws controls
  textSize(40);
  text("W", 180, 200);
  text("A", 100, 330);
  text("D", 275, 330);

  //checks if any key was pressed and if this has been up for more than a second
  if ((forward||left||right) && dead_timer >= 60) {

    death_screen = false; 

    //resets health and score
    player.health = 3; 
    score  = 0;
  }
}
