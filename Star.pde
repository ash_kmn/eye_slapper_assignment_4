//declares colors and variables used by stars
color star_color = #3A5C85;
float max_depth = 10;
float min_depth = 1;
int starcount = 1000;


class Star{
  
  PVector position; //raw position in world
  float depth; //distance from camera
  float size; 
  
  Star (){
    position = new PVector(random(playarea), random(playarea));
    depth = random(min_depth,max_depth);
    size = random(10,20);
  }

  void render(PVector camera_pos){
    
    fill(star_color);
    noStroke();
    
    //calculates on screen position with paralax
    float drawpos_x = (position.x-camera_pos.x/depth);
    float drawpos_y = (position.y-camera_pos.y/depth);
    
    //calculates size based on depth
    float length = size/depth;
    
    //draws the star
    beginShape();
    vertex(drawpos_x+length,drawpos_y);
    vertex(drawpos_x+length/4,drawpos_y+length/4);
    vertex(drawpos_x,drawpos_y+length);
    vertex(drawpos_x-length/4,drawpos_y+length/4);
    vertex(drawpos_x-length,drawpos_y);
    vertex(drawpos_x-length/4,drawpos_y-length/4);
    vertex(drawpos_x,drawpos_y-length);
    vertex(drawpos_x+length/4,drawpos_y-length/4);
    endShape(CLOSE);
  }  
  
}
