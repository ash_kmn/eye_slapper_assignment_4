class Explosion {
  int frame = 0; //frames since spawn
  PVector pos; //world position
  boolean timetodie = false; // deletion flag

  Explosion(PVector where) {
    pos = new PVector(where.x, where.y);
    frame = 0;
  }

  void render( PVector camera_pos) {

    //calculates position on screen
    PVector origin = new PVector(pos.x-camera_pos.x, pos.y-camera_pos.y);

    //draws things based on what frame it is
    if (frame == 0) {
      // black circle
      fill(#000000); 
      ellipse(origin.x, origin.y, 100, 100);
    } 
    
    if (frame == 1) {
      {
        // white circle
        fill(#ffffff); 
        ellipse(origin.x, origin.y, 100, 100);
      }
    } 
    
    if (frame == 2) {
      //flags for deletion
      timetodie = true;
    } 

    frame ++;
  }
}
