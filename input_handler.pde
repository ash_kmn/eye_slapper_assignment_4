//input handler variables
boolean forward = false;
boolean boost = false;
boolean left = false;
boolean right = false;

void keyPressed() {
  if (key == 'w') {
    forward = true;
  }

  if (key == 'a') {
    left = true;
  }

  if (key == 'd') {
    right = true;
  }
}

void keyReleased() {
  if (key == 'w') {
    forward = false;
  }

  if (key == 'a') {
    left = false;
  }

  if (key == 'd') {
    right = false;
  }
}
