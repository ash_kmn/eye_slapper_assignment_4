//declares variables and colors used by enemies
color fins = #F8993A;
color enemy_body = #AF5D23;
color enemy_eye = #FFFDF0;
color enemy_iris = #431E1E;
int enemycount = 20;


class Enemy {

  PVector pos;
  PVector fin_dir; //direction of fins
  PVector fin_perp; //direction of fins rotated 90deg for convenience
  float fin_speed; //speed at which fins rotate
  float shoot_timeout; //keeps track of time since last shot
  float shoot_timer = 200; //frames required before next shot
  boolean queue_to_die = false;

  Enemy() {
    pos = new PVector(random(playarea), random(playarea));
    //pos = new PVector(player.position.x, player.position.y);
    fin_dir = new PVector(1, 1).normalize();
    fin_perp = new PVector(fin_dir.x, fin_dir.y).rotate(PI/2);
    shoot_timeout = random(0, 100);
    fin_speed = 0.05;
  }

  void render(PVector camera_pos) {

    //on screen position of enemy
    PVector origin = new PVector(pos.x-camera_pos.x, pos.y-camera_pos.y);

    //calculates fin rotation speed to indicate the current shot windup
    fin_speed = (shoot_timeout/shoot_timer-0.3)*0.2;

    //moves fins
    fin_dir.rotate(fin_speed);

    //calculates the perpendicular to the direction of fins
    fin_perp = new PVector(fin_dir.x, fin_dir.y).rotate(PI/2);

    //draws fins
    fill(fins);
    beginShape();
    vertex(origin.x + 10*fin_dir.x, origin.y + 10*fin_dir.y);
    vertex(origin.x + 25*fin_dir.x +15*fin_perp.x, origin.y + 25*fin_dir.y +15*fin_perp.y);
    vertex(origin.x + 15*fin_dir.x + 25*fin_perp.x, origin.y + 15*fin_dir.y + 25*fin_perp.y);
    vertex(origin.x + 10*fin_perp.x, origin.y + 10*fin_perp.y);
    vertex(origin.x - 15*fin_dir.x + 25*fin_perp.x, origin.y - 15*fin_dir.y + 25*fin_perp.y);
    vertex(origin.x - 25*fin_dir.x +15*fin_perp.x, origin.y - 25*fin_dir.y +15*fin_perp.y);
    vertex(origin.x - 10*fin_dir.x, origin.y - 10*fin_dir.y);
    vertex(origin.x - 25*fin_dir.x - 15*fin_perp.x, origin.y - 25*fin_dir.y - 15*fin_perp.y);
    vertex(origin.x - 15*fin_dir.x - 25*fin_perp.x, origin.y - 15*fin_dir.y - 25*fin_perp.y);
    vertex(origin.x - 10*fin_perp.x, origin.y - 10*fin_perp.y);
    vertex(origin.x + 15*fin_dir.x - 25*fin_perp.x, origin.y + 15*fin_dir.y - 25*fin_perp.y);
    vertex(origin.x + 25*fin_dir.x -15*fin_perp.x, origin.y + 25*fin_dir.y -15*fin_perp.y);
    endShape(CLOSE);

    //draws main body
    fill(enemy_body);
    ellipse(origin.x, origin.y, 40, 40);
    fill(enemy_eye);
    ellipse(origin.x, origin.y, 30, 30);

    //calculates where the eye should look
    PVector eyedir = new PVector(pos.x, pos.y).sub(player.position).normalize().mult(7.5).rotate(PI);

    //draws eye
    fill(enemy_iris);
    float playerdist = new PVector(player.position.x - pos.x, player.position.y - pos.y).mag();
    if (playerdist < 150) {

      //eye turns small when player is too close to shoot
      ellipse(origin.x+eyedir.x, origin.y+eyedir.y, 7, 7);
    } else {

      //draws eye at regular size
      ellipse(origin.x+eyedir.x, origin.y+eyedir.y, 15, 15);
    }
  }

  void shoot() {

    //looks at how far the player is
    float playerdist = new PVector(player.position.x - pos.x, player.position.y - pos.y).mag();

    //doesnt shoot if they are too close, too far, or the shot isnt charged yet
    if (shoot_timeout>= shoot_timer && playerdist < 300 && playerdist > 150) {

      //creates a bullet object
      bullets.add(new Bullet(new PVector(player.position.x-pos.x, player.position.y-pos.y).normalize(), new PVector(pos.x, pos.y)));

      //resets charge
      shoot_timeout = 0;
    } else {

      //charges shot
      shoot_timeout = min(shoot_timeout+1, shoot_timer);
    }
  }

  void collide() {
    if (new PVector(pos.x, pos.y).sub(player.position).mag()< 60) {

      //dies if it is hit by player
      queue_to_die = true;

      //gives player health
      player.health += 0.5;

      //spawns an explosion on own position
      explosions.add(new Explosion(pos));

      //induces screenshake
      camera.slap();

      //increases score and activates the score text effect
      score ++;
      text_size += 20;
    }
  }
}
