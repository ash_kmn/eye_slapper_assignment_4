/*
                      _                             
                     | |                            
  ___ _   _  ___  ___| | __ _ _ __  _ __   ___ _ __ 
 / _ \ | | |/ _ \/ __| |/ _` | '_ \| '_ \ / _ \ '__|
|  __/ |_| |  __/\__ \ | (_| | |_) | |_) |  __/ |   
 \___|\__, |\___||___/_|\__,_| .__/| .__/ \___|_|   
       __/ |                 | |   | |              
      |___/                  |_|   |_|              
========================================================= 
assignment 3 - object oriented toy
made by: Lev Kamenskyi
 */

//declaring all lists of objects
ArrayList<Star> stars = new ArrayList<Star>();
ArrayList<Enemy> enemies  = new ArrayList<Enemy>();
ArrayList<Bullet> bullets  = new ArrayList<Bullet>();
ArrayList<Explosion> explosions  = new ArrayList<Explosion>();

//declaring unique objects
Player player = new Player();
Camera camera = new Camera();

//declaring general colors for later use
color background = #241E44;
color health_full = #B9ED5E;
color health_empty = #09493F;
color text = #97DBD2;

//defining how big the playfield used is
float playarea = 2000;


void setup() {

  //render parameters
  frameRate(60);
  size(400, 400);
  smooth(0);

  //adds stars to the sky
  for (int i = 0; i < starcount; i ++) {
    stars.add(new Star());
  }

  //adds a starting set of enemies
  for (int i = 0; i < enemycount; i ++) {
    enemies.add(new Enemy());
  }
}

void draw() {

  //checks if the game over screen should be drawn
  if (death_screen) {
    draw_death_screen();
  } else {

    background(background);
    camera.move();

    //renders stars
    for (int i = 0; i < stars.size(); i ++) {
      stars.get(i).render(camera.position);
    }

    //renders enemies and executes their logic
    for (int i = 0; i < enemies.size(); i ++) {
      enemies.get(i).render(camera.position);
      enemies.get(i).shoot();
      enemies.get(i).collide();
      if (enemies.get(i).queue_to_die) {

        //if enemy was flagged for delition - deltes that enemy and spawns 2 new ones in place of it
        enemies.remove(i);
        enemies.add(new Enemy());
        enemies.add(new Enemy());
      }
    }

    //renders and executes the logic for bullets
    for (int i = 0; i < bullets.size(); i ++) {
      bullets.get(i).render(camera.position);
      bullets.get(i).move_and_collide();

      //removes bullets queued for deletion
      if (bullets.get(i).queue_for_deletion) {
        bullets.remove(i);
      }
    }

    //renders explosions and deletes old ones
    for (int i = 0; i < explosions.size(); i ++) {
      explosions.get(i).render(camera.position);
      if (explosions.get(i).timetodie) {
        explosions.remove(i);
      }
    }

    //executes player logic
    player.move(camera.position);
    player.render(camera.position);

    draw_ui();
  }
}
