//declares variables used by bullets
color bullet_inner = #FFFDF0;
color bullet_outer = #7DF2CF;


class Bullet {

  PVector pos;//world position
  PVector sppos;//world position 1 frame ago
  PVector speed;
  float def_speed = 10;//speed in pixels per frame

  boolean queue_for_deletion = false;

  Bullet(PVector dir_, PVector pos_) {

    pos = new PVector(pos_.x, pos_.y);
    speed = new PVector(dir_.x, dir_.y).mult(def_speed);
    sppos = new PVector(pos.x, pos.y);
  }

  void move_and_collide() {

    pos.add(speed);

    if (new PVector(pos.x, pos.y).sub(player.position).mag()< 30) {

      //deletes bullet and deals damage if it hit
      player.hit = true;
      queue_for_deletion = true;
    } else if (new PVector(pos.x, pos.y).sub(player.position).mag() >600 ) {

      //deletes bullet if it flew too far away
      queue_for_deletion = true;
    }
  }

  void render(PVector camera_pos) {

    //onscreen position of bullet
    PVector origin = new PVector(pos.x-camera_pos.x, pos.y-camera_pos.y);

    //draws outer part
    stroke(bullet_outer);
    strokeWeight(20);
    line(origin.x, origin.y, sppos.x-camera_pos.x, sppos.y-camera_pos.y);

    //draws white inner part
    stroke(bullet_inner);
    strokeWeight(10);
    line(origin.x, origin.y, sppos.x-camera_pos.x, sppos.y-camera_pos.y);

    sppos = new PVector(pos.x, pos.y); //updates previous frame position

    noStroke();
  }
}
