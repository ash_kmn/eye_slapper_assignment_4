//declares variables used by ui elements
int score = 0;
float text_size = 30; //size of text displaying score


void draw_ui() {
  
  //draw outer part of healthbar
  stroke(health_empty);
  strokeWeight(20);
  line(camera.shake_offset.x+15, camera.shake_offset.y+15, camera.shake_offset.x+115, camera.shake_offset.y + 15);
  
  //draw inner part of health bar
  stroke(health_full);
  strokeWeight(15);
  line(camera.shake_offset.x+15, camera.shake_offset.y+15, camera.shake_offset.x+15+(max(player.health, 0)/3*100), camera.shake_offset.y + 15);
  noStroke();
  
  //displays score
  textSize(30);
  fill(text);
  text("score:", 15, 385);
  textSize(text_size);
  text_size = max(text_size-1, 30);
  text(score, 110, 385);
}
