//declares colors used by player
color flame_big = #F4680B;
color flame_big_charged = #2AC0F2;
color flame_small = #F4C047;
color flame_small_charged = #7DF2CF;
color player_body = #FFFDF0;
color body_damage = #F45D92;


class Player { 

  int dash_timeout = 60; //dash recharge length
  int dash_timer = 0; //time since last dash
  int iframes = 0; //frames of invinvibility left
  int maxhealth = 3; 
  float health = maxhealth;
  float fw_speed = 10; //speed when moving forward
  float strafe_speed = 10; //side dash speed
  float speed_lerp = 0.03; //speed of speed change 
  PVector position; 
  PVector speed;
  PVector direction; //direction forward
  PVector flame_scale;
  boolean hit = false; //was player hit last frame

  Player () {
    position = new PVector(playarea/2+200, playarea/2+200);
    health = maxhealth;
    speed = new PVector(0, 0);
    direction = new PVector(0, -1);
  }

  void render(PVector camera_pos) {

    //setup direction vectors for rendering
    PVector perpendicular = new PVector(direction.x, direction.y);
    perpendicular.rotate(PI/2);

    //calculates screen position
    PVector origin = new PVector(position.x-camera_pos.x, position.y-camera_pos.y);

    //draws the flames
    float current_flame_scale = speed.mag()/fw_speed*random(3, 5);

    //big flame, color based on availability of dash
    if (dash_timer >= dash_timeout) {
      fill(flame_big_charged);
    } else {
      fill(#F4680B);
    }
    beginShape();
    vertex(origin.x, origin.y);
    vertex(origin.x - direction.x*current_flame_scale*5 + perpendicular.x*10, origin.y - direction.y*current_flame_scale*5 + perpendicular.y*10);
    vertex(origin.x - direction.x*4*current_flame_scale*5, origin.y - direction.y*4*current_flame_scale*5);
    vertex(origin.x - direction.x*current_flame_scale*5 - perpendicular.x*10, origin.y - direction.y*current_flame_scale*5 - perpendicular.y*10);
    endShape(CLOSE);

    //small flame
    if (dash_timer >= dash_timeout) {
      fill(flame_small_charged);
    } else {
      fill(flame_small);
    }
    beginShape();
    vertex(origin.x, origin.y);
    vertex(origin.x - direction.x*current_flame_scale*5 + perpendicular.x*5, origin.y - direction.y*current_flame_scale*5 + perpendicular.y*5);
    vertex(origin.x - direction.x*2*current_flame_scale*5, origin.y - direction.y*2*current_flame_scale*5);
    vertex(origin.x - direction.x*current_flame_scale*5 - perpendicular.x*5, origin.y - direction.y*current_flame_scale*5 - perpendicular.y*5);
    endShape(CLOSE);

    //draws the ship, color based on invinvibility
    fill(player_body);
    if (iframes >0) {
      fill(body_damage);
    }
    beginShape();
    vertex(origin.x + direction.x*20, origin.y + direction.y*20);
    vertex(origin.x - direction.x*10 + perpendicular.x*15, origin.y - direction.y*10 + perpendicular.y*15);
    vertex(origin.x - direction.x, origin.y - direction.y);
    vertex(origin.x - direction.x*10 - perpendicular.x*15, origin.y - direction.y*10 - perpendicular.y*15);
    endShape(CLOSE);
  }

  void move(PVector camera_pos) {

    //position of mouse in world coordinates
    PVector worldcursor = new PVector(mouseX, mouseY).add(camera_pos);

    //direction to cursor
    worldcursor.sub(position).normalize();
    direction = worldcursor;
    PVector perpendicular = new PVector(direction.x, direction.y);
    perpendicular.rotate(PI/2);

    //sets desired speed
    PVector speed_target = new PVector(0, 0);
    if (forward) {
      speed_target.x += direction.x*fw_speed; 
      speed_target.y += direction.y*fw_speed;
    }

    //instantly changes speed if player dashed right
    if (right && (dash_timer >= dash_timeout)) {
      speed.x += perpendicular.x*strafe_speed; 
      speed.y += perpendicular.y*strafe_speed; 
      dash_timer = 0;
    } else {
      //recharges dash
      dash_timer++;
    }

    //changes speed if dashed left
    if (left && (dash_timer >= dash_timeout)) {
      speed.x -= perpendicular.x*strafe_speed; 
      speed.y -= perpendicular.y*strafe_speed; 
      dash_timer = 0;
    }

    //shifts speed to be closer to target speed
    speed.x = lerp(speed.x, speed_target.x, speed_lerp);
    speed.y = lerp(speed.y, speed_target.y, speed_lerp);

    //moves player
    position.add(speed);

    //checks if player was hit
    if (hit && iframes <= 0) {
      hit = false; 
      health --; 
      iframes = 10;
    } else {
      iframes = max(iframes-1, 0);
    }

    //caps health to prevent healing above 3
    health = min(health, 3);

    //triggers game over screen if health is 0
    if (health<=0) {
      death_screen = true; 
      dead_timer = 0;
    }
  }
}
